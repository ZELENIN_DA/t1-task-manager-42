package ru.t1.dzelenin.tm.api.service;

import ru.t1.dzelenin.tm.api.repository.IUserOwnedRepository;
import ru.t1.dzelenin.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnerService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedRepository<M>, IService<M> {

}