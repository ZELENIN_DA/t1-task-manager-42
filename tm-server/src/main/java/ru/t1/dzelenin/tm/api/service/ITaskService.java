package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.model.TaskDTO;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.enumerated.TaskSort;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnerService<TaskDTO> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId, @Nullable TaskSort sort);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    TaskDTO updateById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable String name,
                       @Nullable String description);

    @NotNull
    TaskDTO changeTaskStatusId(@Nullable String userId, @Nullable String id, @Nullable Status status);

}


