package ru.t1.dzelenin.tm.service;

import org.junit.experimental.categories.Category;
import ru.t1.dzelenin.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class SessionServiceTest {
/*
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Test
    public void add() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionService.findAll().get(0));
    }

    @Test
    public void addList() {
        Assert.assertNotNull(sessionService.add(ADMIN1_SESSION_LIST));
        for (final Session session : ADMIN1_SESSION_LIST)
            Assert.assertEquals(session, sessionService.findOneById(session.getId()));
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionService.findAll().get(0));
        Assert.assertEquals(USER1.getId(), sessionService.findAll().get(0).getUserId());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionService.findAll());
        sessionService.removeOneById(USER2.getId());
        Assert.assertFalse(sessionService.findAll().isEmpty());
        sessionService.removeOneById(USER1.getId());
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER2_SESSION1);
        sessionService.removeOneById(USER1.getId());
        Assert.assertFalse(sessionService.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionService.findAll(USER1.getId()));
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1_SESSION1);
        sessionService.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionService.findOneById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertNotEquals(USER2_SESSION1, sessionService.findOneById(USER1.getId(), USER2_SESSION1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1_SESSION1);
        sessionService.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionService.removeOne(USER1.getId(), USER1_SESSION1));
        Assert.assertFalse(sessionService.findAll().contains(USER1_SESSION1));
        Assert.assertTrue(sessionService.findAll().contains(USER2_SESSION1));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1_SESSION1);
        sessionService.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionService.removeOneById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertFalse(sessionService.findAll().contains(USER1_SESSION1));
        Assert.assertTrue(sessionService.findAll().contains(USER2_SESSION1));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(sessionService.findAll().isEmpty());
        sessionService.add(USER1_SESSION1);
        Assert.assertTrue(sessionService.existsById(USER1_SESSION1.getId()));
        Assert.assertFalse(sessionService.existsById(USER2_SESSION1.getId()));
    }
*/
}
