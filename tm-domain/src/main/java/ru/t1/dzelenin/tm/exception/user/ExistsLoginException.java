package ru.t1.dzelenin.tm.exception.user;

import org.jetbrains.annotations.NotNull;

public class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login alredy exists...");
    }

    public ExistsLoginException(@NotNull final String login) {
        super("Error! Login '" + login + "' alredy exists...");
    }

}
